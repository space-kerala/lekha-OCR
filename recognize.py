# -*- coding: utf-8 -*-
import cv2
import preprocess as pp
import training as train
label_uni=train.label_uni
def recognize_letter(feature):
	a = train.classifier.predict(feature.reshape(1,-1))
	# if (label_uni[int(a)]=='0' or label_uni[int(a)]=='ഠ'):
	# 	if(pp.previous_char==None):
	# 		return label_uni.index('0')
	# 	if(label_uni[int(pp.previous_char.label)].isdigit()):
	# 		return label_uni.index('0')
	# 	if(pp.cur_char.height<=(pp.previous_char.height*3/4)):
	# 		return label_uni.index('ം')
	# 	return label_uni.index('ഠ')
	return a[0]
def recognize(url):
	# print (url)
	img=cv2.imread(url,0)
	if(img.data==None):
		print url+' does\'nt exist'
		exit()
	img = pp.preprocess(img)
	im=img
	im,rot = pp.skew_correction(img)
	# Add Layout analysis here
	print recognize_block(im)
'''Recgnizing a block of scanned image'''
def recognize_block(im):
	line = pp.find_lines(im)
	# print len(linene)
	label_list=label_uni
	i=0
	string=''
	#selecting each line
	for l in line:
		# cv2.imwrite('temp/zline_temp_'+str(i)+'.png',l.data)
		string=string+'\n'
		#selecting words in a line
		for w in l.word_list:
			#cv2.imwrite('zword_temp_'+str(i)+'_word_'+str(j)+'.png',w.data)
			string=string+' '
			list=[]
			for char in w.char_list:
				list.append(label_list[int(char.label)])
			string+=form_word(list)
	return string

#Formatting characters in the word
def form_word(list):
	i=0
	string=''
	while(i<len(list)):
		try:
			#checking whether the input is  ' or " or ,
			if(list[i]in ['\'',',']):
				if(list[i+1] in ['\'',',']):
					string=string+'\"'
					i+=1
				else:
					string=string+list[i]
			#checking whether the input is  ൈ  or െ
			elif(list[i]in ['െ','േ','്ര']):
				try:
					if(list[i+1]in ['െ','്ര']):
						string=string+list[i+2]+list[i+1]+list[i]
						i+=3
						continue
					elif(list[i+2]in ['്വ']):
						string=string+list[i+1]+list[i+2]+list[i]
						i+=3
						continue
				except IndexError:
					do_nothing=0
				string=string+list[i+1]
				string=string+list[i]
				i+=1
			else:
				string=string+list[i]
		except IndexError:
			string=string+list[i]
		i+=1
	return string
# list=['അ','േ','ന','്വ','ഷ','ി','ച്ച','്']
# print form_word(list)
