import glob
import cv2
import preprocess as pp
import training as train
def update_box():
	f=open('./corrected_docs/doc_2/compare_list.txt','r')
	cor_list=f.readlines()
	f2=open('./corrected_docs/doc_2/dummy.box','r')
	dummy_list=f2.readlines()
	f3=open('./corrected_docs/doc_2/dummy_cor.box','w')
	dummy_cor=""
	j=0
	for i in dummy_list:
		i2=''
		flag=0
		for k in i:
			if(k=='\t'):
				flag=1
			if (flag==1):
				i2+=k
		dummy_cor+=cor_list[j][:-1]+'\t'+i2
		j+=1
	print len(cor_list)
	print len(dummy_list)
	f3.write(dummy_cor)
# update_box()

def test():
	# load()
	count,correct=0,0
	url='../samples/train_images/'
	for i in range(101,150):
		s_list=glob.glob(url+str(i)+'/*.png')
		for j in s_list:
			imgo=cv2.imread(j,0)
			img=pp.preprocess(imgo.copy())
			f = find_feature(img.copy())
			fu= np.array(f,np.float32)
			# print len(fu)
			t = classifier.predict(fu)
			print label_uni[i-100],label_uni[int(t)],int(i-100==t)
			if(i-100==t):
				correct+=1
			else:
				name = './zerr_'+str(i)+'_'+str(count)+'.png'
				print j
				print count
				# cv2.imwrite('./temp/zerr_temp_'+str(i)+'_'+str(count)+'z.png',img)
				shutil.copyfile(j,name)
			count+=1
	print 'accuracy :'+str(100.0*correct/count)+'%'
	print ('accurate recognition :'+str(correct))
	print ('total character tested :'+str(count))
def gen_train_sample(im):
	# train.classifierclassifier.load('svm_class.xml')
	img = pp.preprocess(im.copy())
	# img,rot = pp.skew_correction(img)
	hight,width=im.shape
	# M = cv2.getRotationMatrix2D((hight/2,width/2),rot-90,1)
	# im = cv2.warpAffine(im,M,(width,hight))
	# cv2.imwrite('skew_temp_correct.png',im)
	contours2, hierarchy = cv2.findContours(img,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
	contours = []
	for cnt in contours2:
		print (cv2.contourArea(cnt))
		if(cv2.contourArea(cnt)>20):
			contours.append(cnt)
	X = [cv2.contourArea(C) for C in contours]
	# print len(contours),len(X)
	t=[i for i in range (0,len(contours))]
	X,t = zip(*sorted(zip(X,t)))
	i=0
	for j in t:
		x,y,w,h=cv2.boundingRect(contours[j])
		box = im[y-1:y+h+1,x-1:x+w+1]
		char = pp.preprocess(box.copy())
		try:
			f = train.find_feature(char)
			fu= train.np.array(f,train.np.float32)
			# print len(fu)
			t = train.classifier.predict(fu)
			print t
		except IndexError:
			t = 0
		cv2.imwrite('samp/zsamp47_temp8_'+str(int(t))+'_'+str(i)+'.png',box)
		# cv2.imwrite('./samp/'+str(i)+'.png',box)
		i+=1
tno=22
def make_compare_file():
	f=open('./corrected_docs/Samp_'+str(tno)+'/compare_list_new.txt','w')
	g=open('./corrected_docs/Samp_'+str(tno)+'/output_file_new.txt','w')
	# img=cv2.imread('./Example/dc_books_page.png',0)
	path='./corrected_docs/Samp_'+str(tno)+'/*.jpeg'
	url=glob.glob(path)
	img=cv2.imread(url[0],0)
	# img=cv2.imread('./Samp_3/samp3.png',0)
	if(img==None):
		print 'image does\'nt exist'
		exit()
	img = pp.preprocess(img)
	# im=img
	# im,rot = pp.skew_correction(img)

	line = pp.find_lines(img.copy())
	# print len(linene)
	label_list=train.label_unicode()
	i=0
	num=[]
	for l in line:
		for w in l.word_list:
			for c in w.char_list:
				# num.append((str(i),label_list[int(c.label)]))
				tup=label_list[int(c.label)]
				f.write(tup+'\n')
				g.write(tup)
				# cv2.imwrite('./Samp_22/samp/'+str(i)+'.png',c.data)
				i+=1
			g.write(' ')
		g.write('\n')
	f.close()
	g.close()
	# for tup in num:
	# 	# f.write(tup[0]+' '+tup[1]+'\n')
	# 	f.write(tup[1]+'\n')
	# f.close()
def calculate_accuracy():
	f=open('./corrected_docs/Samp_'+str(tno)+'/compare_list.txt','r')
	g=open('./corrected_docs/Samp_'+str(tno)+'/compare_list_new.txt','r')
	h=open('./result_compare.txt','w')
	l1=f.readlines()
	l2=g.readlines()
	# list1=[231,376,435,623,876,892,952,961,1002,1034,1036,1100,1155]
	j=0
	k=0
	for j in range(len(l1)):
		# for line2 in l2:
		a1=str(l1[j][:-1])
		a2=str(l2[j][:-1])
		# print a1+':'+a2
		if a1==a2:
			continue
		else:
			# print str(j)+':'+a1+'and'+a2
			# if j in list1:
			# 	print str(j)+':'+a1+'and'+a2
			h.write(str(j)+' '+a2+' '+a1+'\n')
			k+=1
	print 'ERRORS:',k
	print 'TOTAL:',j+1
	print 'ACCURACY:',100.0-((k/(j+1.0))*100.0)
# for tno in range(1,17):
# print 'DOC_'+str(tno)
# make_compare_file()
# calculate_accuracy()

def make_modified_file():
	f=open('./compare_list.txt','r')
	g=open('./output_file.txt','w')
	img=cv2.imread('./Example/dc_books_page.png',0)

	if(img==None):
		print url+' does\'nt exist'
		exit()
	img = pp.preprocess(img)
	im,rot = pp.skew_correction(img)

	line = pp.find_lines(im.copy())
	# print len(linene)
	label_list=train.label_unicode()

	q=f.readlines()
	i=0
	num=[]
	for l in line:
		for w in l.word_list:
			for c in w.char_list:
				# num.append((str(i),label_list[int(c.label)]))
				tup=label_list[int(c.label)]
				if(q[i][:-1]!=tup):
					print tup
				# f.write(tup+'\n')
				g.write(tup)
				# cv2.imwrite('samp/'+str(i)+'_temp_.png',c.data)
				i+=1
			g.write(' ')
		g.write('\n')
	f.close()
	g.close()
