# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 10:22:39 2015
@author: james
"""
#Preprocess and Segmentation
import cv2
import numpy as np
import recognize
import training as train
import features
import glob
# current_line = 0
# current_word = 0

previous_char = None
cur_char = None
BOX_SIZE = 128

def preprocess(img):#eliptical kernel
	"""Does adaptive thresholding to the image.Converts it into a binary image"""
	# cv2.imwrite('before_temp_thresholding.png',img)
	img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,243,50)
	# cv2.imwrite('after_temp_thresholding.png',img)
	return img
# Read more : http://docs.opencv.org/3.1.0/d7/d4d/tutorial_py_thresholding.html

def skew_correction(img):
	height,width=img.shape
	if(height>1200 and width>1200):
		box = img[height/2-600:height/2+600,width/2-600:width/2+600]
		height,width=box.shape
	else:
		box = img
	# Canny : Extracts the edges from the image
	# Read More : http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/canny_detector/canny_detector.html
	# HoughLines : Gets the lines from the image
	# http://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/hough_lines/hough_lines.html
	edges = cv2.Canny(box,50,150,apertureSize = 3)
	lines = cv2.HoughLines(edges,1,np.pi/360,width/5,width/2,height/10)
	# print lineskernel3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
	count=0
	rotation=[]
	for rho,theta in lines[0]:
		angle = theta*180.0/np.pi
		if (angle<60 or angle >120):
			continue
		rotation.append(angle)
		count+=1
		if(count == 50):
			break
	#	a = np.cos(theta)
	#	b = np.sin(theta)
	#	x0 = a*rho
	#	y0 = b*rho
	#	x1 = int(x0 + 3000*(-b))
	#	y1 = int(y0 + 3000*(a))
	#	x2 = int(x0 - 3000*(-b))
	#	y2 = int(y0 - 3000*(a))
	#	cv2.line(img,(x1,y1),(x2,y2),200,2)
	rotation.sort()
	rot=rotation[len(rotation)/2]
	height,width=img.shape
	M = cv2.getRotationMatrix2D((height/2,width/2),rot-90,1)
	dst = cv2.warpAffine(img,M,(width,height))
	return dst,rot
char_id=1
f=open('../lekha-OCR-database/corrected_docs/doc_1/compare_list.txt','r')
cor_list=f.readlines()
box_str=""
def center_box(img,cnt):
	"""Finds the bounding rectangle of the given set of countours cnt"""
	# Read More : http://docs.opencv.org/master/da/d0c/tutorial_bounding_rects_circles.html
	x,y,w,h=cv2.boundingRect(cnt)
	char=img[y-1:y+h+1,x-1:x+w+1]
	global w_vpos,w_hpos,toXml,char_id,box_str,im_h,cor_list
	toXml+='\n\t\t<CHAR=\"'+str(char_id)+'\" HPOS = \"'+str(w_hpos+x-1)+'\" VPOS =\"'+str(w_vpos+y-1)+'\" '
	toXml+='HIGHT =\"'+str(h+2)+'\" WIDTH =\"'+str(w+2)+'\" '
	box_str=str(w_hpos+x-1)+'\t'+str(im_h-(w_vpos+y-1+h+2))+'\t'+str(w_hpos+x-1+w+2)+'\t'+str(im_h-(w_vpos+y-1))+'\t0\n'+box_str
	# box_str=box_str+cor_list[char_id-1][:-1]+'\t'+str(w_hpos+x-1)+'\t'+str(im_h-(w_vpos+y-1+h+2))+'\t'+str(w_hpos+x-1+w+2)+'\t'+str(im_h-(w_vpos+y-1))+'\t0\n'
	char_id+=1
	return char

b_vpos,b_hpos=0,0 #block cordinates
total=""
toXml="<-- Lekha-OCR -->\n"
im_h=im_w=0
l_vpos,l_hpos=0,0 #line cordinates
def find_lines(url):
	"""Finds lines by taking the average pixel deinsity and cutting based on max_cuts"""
	img=cv2.imread(url,0)
	if(img.data==None):
		print url+' does\'nt exist'
		exit()
	img = preprocess(img)
	# cv2.imwrite('temp_img_in.png',img)
	im=img
	# im,rot = skew_correction(img)
	# print rot
	line_list =[]
	# return
	height,width=img.shape
	f1 = open('dummy.xml','w')
	f2 = open('dummy.box','w')
	global toXml,l_vpos,l_hpos,b_hpos,b_vpos,box_str,im_h,im_w,total
	im_h=height
	im_w=width
	toXml += '<IMAGE =\"'+str(url)+'\" HEIGHT = \"'+str(height)+'\" '
	toXml += 'WIDTH = \"'+str(width)+'\" >\n'


	hor_pix_den=[0 for i in range(0,height)]
	for i in range(0,height):
		for j in range(0,width):
			hor_pix_den[i]+=img[i,j]
		hor_pix_den[i]/=255
	# print hor_pix_den
	max_cuts = 8
	min_line_width = 20
	j,start=0,0
	line_id=1
	for i in range (0,height):
		j = i
		if(hor_pix_den[i]<max_cuts):
			if(min_line_width>j-start):
				start = j
			else :
				# print start,j
				toXml += '<LINE=\"'+str(line_id)+'\" '
				l_hpos=b_hpos+0
				l_vpos=b_vpos+start-1
				toXml += 'HPOS =\"'+str(b_hpos+0)+'\" '
				toXml += 'VPOS =\"'+str(b_vpos+start-1)+'\" '
				toXml += 'HEIGHT =\"'+str(j-start+2)+'\" '
				toXml += 'WIDTH =\"'+str(width)+'\">\n'
				line = Line(img[start-1:j+1,0:width])
				toXml+='</LINE>\n'
				total+='\n'
				line_id+=1
				line_list.append(line)
				start = j
	# print line_list
	# toXml+=total+'</IMAGE>\n'
	toXml+='</IMAGE>\n'
	f1.write(toXml)

	nbox_str=""
	nbox_str2=""
	for i in box_str:
		nbox_str+=i
		if (i=='\n'):
			nbox_str2=nbox_str+nbox_str2
			nbox_str=""
	f2.write(nbox_str2)
	return line_list

class Line:
	"""Cuts the blocks into lines.Then sends it to be cut as words"""
	no_words = 0
	def __init__(self,img):
		self.data = img
		self.sw = find_sw(img)
		self.word_list=find_words(img)
w_vpos=w_hpos=0
char_id2=0
def find_words(img):
	global toXml,l_vpos,l_hpos,w_vpos,w_hpos,total
	toXml+='\n'
	global previous_char,char_id2
	height,width=img.shape
	word_list = []
	ver_pix_den=[0 for i in range(0,width)]
	for i in range(0,width):
		for j in range(0,height):
			ver_pix_den[i]+=img[j,i]
		ver_pix_den[i]/=255
	print (i for i in ver_pix_den);
	max_cuts = 0
	min_word_sep = height/5
	j,i,start=0,0,0
	word_id=1
	while(i<width):
		if (ver_pix_den[i]<=max_cuts):
			j = i
			while(ver_pix_den[j]<=max_cuts and j<width-1):
				j+=1
			if(j-i>min_word_sep):
				if(i-start>min_word_sep):
					previous_char=None
					w_hpos=l_hpos+start
					w_vpos=l_vpos+0
					toXml+= '\t<WORD= \"'+str(word_id)+'\" HPOS= \"'+str(l_hpos+start)+'\" VPOS=\"'+str(l_vpos+0)
					toXml+='\" HIGHT = \"'+str(height)+'\" WIDTH=\"'+str((i+1)-start)+'\" >'
					word = Word(img[0:height,start:i+1])
					list=[]
					# list2=[]
					for char in word.char_list:
						list.append(train.label_uni[int(char.label)])
						# list2.append(cor_list[char_id2][:-1])
						# char_id2+=1
					toXml+=recognize.form_word(list)
					# total+=recognize.form_word(list2)+" "
					toXml+='\t</WORD>\n'
					word_id+=1
					word_list.append(word)
				start=j
				i=j
			else:
				i=j
		i+=1
	if(i-start>min_word_sep):
		word = Word(img[0:height,start-1:i+1])
		word_list.append(word)
	return word_list

class Word:
	no_letters = 0
	def __init__(self,img):
		self.data = img
		self.char_list = find_lettes(img)

def find_lettes(img):
	global previous_char
	char_list = []
	# cv2.imwrite('temp/temp_word.png',img)
	contours2, hierarchy = cv2.findContours(img.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
	if(len(contours2)==0):
		return char_list
	contours = []
	for cnt in contours2:
		# print (cv2.contourArea(cnt))
		if(cv2.contourArea(cnt)>6):
			try:
				contours.append(cnt)
			except ValueError:
				print ('error')
				pass
	Mset = [cv2.moments(cnt) for cnt in contours]
	X = [int(M['m10']/M['m00']) for M in Mset]
	index = [i for i in range(0,len(contours2))]
	try:
		X,index = zip(*sorted(zip(X,index)))
	except:
		return []
	for i in index:
		cnt = contours[i]
		box = center_box(img.copy(),cnt)
		letter = Letters(box)
		previous_char=letter
		char_list.append(letter)
	return char_list

class Letters:
	ratio = 0
	def __init__(self,char):
		global cur_char,toXml,box_str
		cur_char = self
		self.height,t=char.shape
		self.data=char
		self.feature=np.array(features.find_feature(self.data.copy()),np.float32)
		self.label=recognize.recognize_letter(self.feature)
		toXml+='LABEL =\"'+train.label_uni[int(self.label)]+'\"></CHAR>\n'
		box_str=train.label_uni[int(self.label)]+'\t'+box_str
	# def height(self):
	# 	h,w=self.data.shape
	# 	return h

def find_sw(img):
	"""Function to find stroke width """
	global stroke_width
	# if(stroke_width !=0):
		# recturn stroke_width
	height,width = img.shape
	array = [0 for i in range(0,width/2)]
	for j in range(0,height):
		count = 0
		for i in range (0,width):
			if(img[j,i]==255):
				count+=1
			else:
				array[count]+=1
				count = 0
	array[0]=0
	stroke_width = array.index(max(array))
	return stroke_width

find_lines('Example/dc_books_page.png')

# for i in range(10,20):
# 	url='corrected_docs/doc_'+str(i)+'/'
# 	tiff_list=glob.glob(url+'/*.tiff')
# 	print tiff_list[0]
# 	find_lines(tiff_list[0])
# 	f1=open(url+'dummy.xml','w')
# 	f2=open(url+'dummy.box','w')
# 	f1.write(toXml)
#
# 	nbox_str=""
# 	nbox_str2=""
# 	for i in box_str:
# 		nbox_str+=i
# 		if (i=='\n'):
# 			nbox_str2=nbox_str+nbox_str2
# 			nbox_str=""
# 	f2.write(nbox_str2)
