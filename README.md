*Note: This project is under development. All modules are written in python. For image processing opencv is used, for training SVM of Scikit lern used
# lekha-OCR

Printed text recognizer for Malayalam, Lekha OCR is an optical character recognizer trained for the recognition of printed malayalam Documents.

Prerequirements
======
>OpenCV 2.4.11 (not works if opencv 3.0+)
>
>Python 2.7.9
>
>numpy (>= 1.6.1)
>
>scikit-learn

In debian 8 jessie and ubuntu 14.04
-----------
       #apt-get insatll python-opencv python-pip
       #pip install numpy scikit-learn

Usage
======
Currently *only block recognition* is available. Layout analysis is at developing stage.

To recognize a scanned malayalam document and get the malayalam characters as output.

      $./lekhaocr <filename>
for example

      $./lekhaocr Example/dc_books_page.png

Supporters
=======
This project is developed by [SPACE] (http://www.space-kerala.org/) in association with [ICFOSS] (http://icfoss.in/).

Contributors
=======
**Arun Joseph** contributed most of the engine initial developments.

**Sachin** developing second phase

**Jithin Thankachan** contributed some additional features, training tool and helped in documentation.

**Balagopal Unnikrishnan** contributed in preparing XML label for training and helped in documentation.

**Rijoy V** contributed in initial research.

**Ambily Sreekumar** contributed in building data set for training.

**Arun M** helped in project management and technical assistance.

This project is initially hosted at **[Github] (https://github.com/space-kerala/lekha_OCR_1.0)**
From 2016/April onwards new developments updating to gitlab.

***********************************************************************

***********************************************************************
